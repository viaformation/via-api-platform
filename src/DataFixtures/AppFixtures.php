<?php

namespace App\DataFixtures;

use App\Entity\Article;
use App\Entity\Author;
use App\Entity\Category;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{
    public function __construct(private readonly UserPasswordHasherInterface $passwordHasher)
    {}

    public function load(ObjectManager $manager): void
    {
        $user = new User();
        $user->setEmail('eleve@via.com');
        $user->setPassword($this->passwordHasher->hashPassword($user, 'bonjour'));
        $user->setRoles(['ROLE_ADMIN']);
        $manager->persist($user);

        $faker = Factory::create('fr_FR');

        for($i=0;$i<6;$i++){
            $manager->persist(
                (new Category())->setTitle($faker->realText(50))
                                ->setDescription($faker->paragraph())
                                ->setImage($faker->imageUrl(400,240))
            );
        }
        $manager->flush();

        $categories = $manager->getRepository(Category::class)->findAll();

        $author1 = new Author();
        $author2 = new Author();
        $author3 = new Author();

        $author1->setFirstname($faker->firstName())->setLastname($faker->lastName());
        $author2->setFirstname($faker->firstName())->setLastname($faker->lastName());
        $author3->setFirstname($faker->firstName())->setLastname($faker->lastName());
        $manager->persist($author1);
        $manager->persist($author2);
        $manager->persist($author3);
        $manager->flush();
        $authors = [$author1, $author2, $author3];

        foreach ($categories as $category) {
            for($i=0;$i<10;$i++){
                $n = rand(0,2);
                $manager->persist(
                    (new Article())->setTitle($faker->realText(50))
                                    ->setContent($faker->text(1000))
                                    ->setAuthor($authors[$n])
                                    ->setExcerpt($faker->realText())
                                    ->setDate($faker->dateTime())
                                    ->addCategory($category)
                );
            }
        }
        $manager->flush();
    }
}
